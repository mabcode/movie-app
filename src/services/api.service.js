



let ApiService = class ApiService {
	constructor() {
		this.apiProtocol = 'https://';
		this.apiHost = 'api.themoviedb.org/3/';
		this.apiKey = '2b4f97c0df61ceb085f31e7d552fe78c'
		this.movie = 'now_playing'
        this.page  = '1'
	}

	/*
	* Utility methods/properties
	*/
	get apiLocation() {
		return `${this.apiProtocol}${this.apiHost}`;
	}

	/*
	* API addresses
	*/
	getMovieList() {
		return `${this.apiLocation}movie/${this.movie}?api_key=${this.apiKey}&page=${this.page}`;
    }
    
    getGenreList(){
        return `${this.apiLocation}genre/movie/list?api_key=${this.apiKey}&language=en-US`;
    }

    getMoviePerGenre(id){
        return `${this.apiLocation}discover/movie?api_key=${this.apiKey}&with_genres=${id}`;
	}
	
	getMoviePerID(id){
		return `${this.apiLocation}movie/${id}?api_key=${this.apiKey}&language=en-US`;
	}




};

// Create a Singleton
const apiService = new ApiService();
export default apiService;

// search movie by id
// https://api.themoviedb.org/3/movie/399579?api_key=2b4f97c0df61ceb085f31e7d552fe78c&language=en-U

//get a page of movies
//https://api.themoviedb.org/3/movie/now_playing?api_key=2b4f97c0df61ceb085f31e7d552fe78c&page=1

//get the genres
//https://api.themoviedb.org/3/genre/movie/list?api_key=2b4f97c0df61ceb085f31e7d552fe78c&language=en-US

//discover movies with genre id
//https://api.themoviedb.org/3/discover/movie?api_key=2b4f97c0df61ceb085f31e7d552fe78c&with_genres=28


//https://api.themoviedb.org/3/movie/550?api_key=2b4f97c0df61ceb085f31e7d552fe78c