//-------------------------------------------------------------------
//
// The purpose of this class is to provide a single location that the
// rest of the application can use to obtain URL routes for working with
// the backend API.
//
//-------------------------------------------------------------------
import apiService from './api.service';
import { Movie } from '../models/movie';

let MovieService = class MovieService {
    constructor() {
    }

    getMovies() {
        return new Promise((resolve, reject) => {
            fetch(apiService.getMovieList())
                .then((response) => response.json())
                .then((response) => {
                    let items = [];
                    console.log('response: ', response)
                    response.results.forEach(movie => {
                        items.push(new Movie(movie.title));
                    }

                    )

                    resolve(items);
                })
                .catch((error) => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getMovieInfo(id) {
        return new Promise((resolve, reject) => {
            fetch(apiService.getMoviePerID(id))
                .then((response) => response.json())
                .then((response) => {
                    let items = [];
                    console.log('response: ', response)
                    console.log('The ID inside the class: ', id)
                    response.results.forEach(movie => {
                        items.push(new Movie(movie.title, movie.id, movie.poster_path, movie.popularity, movie.release_date, movie.overview));
                    })

                    resolve(items);
                })
                .catch((error) => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getMoviesPerGenre(id) {
        return new Promise((resolve, reject) => {
            fetch(apiService.getMoviePerGenre(id))
                .then((response) => response.json())
                .then((response) => {
                    let items = [];
                    console.log('response: ', response)
                    console.log('The ID inside the class: ', id)
                    response.results.forEach(movie => {
                        items.push(new Movie(movie.title, movie.id, movie.poster_path, movie.popularity, movie.release_date, movie.overview));
                    })

                    resolve(items);
                })
                .catch((error) => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getGenres() {
        return new Promise((resolve, reject) => {
            fetch(apiService.getGenreList())
                .then((response) => response.json())
                .then((response) => {
                    let items = [];
                    console.log('response: ', response)
                    response.genres.forEach(movie => {
                        items.push(new Movie(movie.name, movie.id));
                    }

                    )
                    resolve(items);
                })
                .catch((error) => {
                    console.error(error);
                    reject(error);
                });
        });
    }
};

// Create a Singleton
const movieService = new MovieService();
export default movieService;
