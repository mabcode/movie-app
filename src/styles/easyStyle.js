import {StyleSheet,Platform} from 'react-native';



export default StyleSheet.create ({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
      },
      welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
      },
      instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
      },
      touchableButton: {
        backgroundColor: 'lightblue',
        padding: 10,
        margin: 10,
        borderRadius: 20
    },
    touchableSmallButton: {
        backgroundColor: 'lightgrey',
        flexWrap: 'wrap',
        flexDirection: 'row',
        padding: 7,
        margin: 7,
        borderRadius: 10
    },
    touchableButtonText: {
        fontSize: 20
    },
    scroll: {
        alignSelf: 'stretch'
    },
    scrollContent: {
        alignItems: 'center'
    },
    inputContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    searchText: {
        borderBottomColor: Platform.OS === 'ios' ? 'black' : null, borderBottomWidth: Platform.OS === 'ios' ? 1 : null,
        width: 230,
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    }

});