/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Alert,
    Platform,
    Text,
    TouchableOpacity,
    View,
    TextInput,
} from 'react-native';
import { NavigationEvents } from 'react-navigation';

import styles from '../styles/easyStyle';

export default class ScreenFive extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Search',
            // headerLeft: <Button
            //     title='Custom'
            //     onPress={navigation.getParam('triggerAlert')}
            // />
        };
    }

    constructor(props) {
        super(props);

        this.state = {
            value: 1000,
            actorSearch: "",
            movieSearch: "",
            person: "Seach for Person",
            movie: "Seach for Movie",
        }
    }

    /*** Mounting ***/
    componentWillMount() {
        console.log('Search: componentWillMount');
    }

    render() {
        return (
            <View style={styles.container}>
                <NavigationEvents
                    onWillBlur={payload => console.log('Search: willBlur', payload)}
                    onDidBlur={payload => console.log('Search: didBlur', payload)}
                    onWillFocus={payload => console.log('Search: willFocus', payload)}
                    onDidFocus={payload => console.log('Search: didFocus', payload)}
                />
                <Text style={styles.welcome}>
                    Welcome to Sreach Screen
                </Text>

                <View style={styles.inputContainer}>
                    <TextInput
                        style={styles.searchText}
                        value={this.state.movie}
                        onChangeText={(val) => this.setState({ movie: val })}
                        returnKeyType='done'
                        clearTextOnFocus={true}
                        onSubmitEditing={(val) => { 
                            this.props.navigation.navigate('MovieSummary',{ 
                                genreID: val,
                                title: "Movie Search Results:"
                            }) 
                        }}
                    />
                    <TextInput
                        style={styles.searchText}
                        value={this.state.person}
                        onChangeText={(val) => this.setState({ person: val })}
                        returnKeyType='done'
                        clearTextOnFocus={true}
                        onSubmitEditing={(val) => { 
                            this.props.navigation.navigate('PersonSummary',{ 
                                genreID: val,
                                title: "People Search Results:"
                            }) 
                        }}
                    />
                </View>
            </View>
        );
    }

    componentDidMount() {
        console.log('Search: componentDidMount');

        this.props.navigation.setParams({ triggerAlert: this.triggerAlert })
    }

    /*** UPDATING ***/
    componentWillReceiveProps(nextProps) {
        console.log('Search: componentWillReceiveProps');
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log('Search: shouldComponentUpdate');
        return true;
    }

    componentWillUpdate(nextProps, nextState) {
        console.log('Search: componentWillUpdate');
    }

    //render() is the next step, but is defined already

    componentDidUpdate(prevProps, prevState) {
        console.log('Search: componentDidUpdate');
    }

    /*** UNMOUNTING ***/
    componentWillUnmount() {
        console.log('Search: componentWillUnmount');
    }

    triggerAlert = () => {
        Alert.alert(
            "State Value",
            "The state value is " + this.state.value,
            [
                { text: 'OK' },
            ]
        );
    }
}
