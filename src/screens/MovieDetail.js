
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    ScrollView,
    RefreshControl,
    FlatList,
} from 'react-native';
import movieService from '../services/movie.service';

import styles from '../styles/easyStyle';

export default class MovieDetail extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id: 0,
            movie: [],
        }
    }

    static navigationOptions = {
        title: 'Movie Detail'
    }

    /*** Mounting ***/
    componentWillMount() {
        console.log('Home: componentWillMount');
        //set Genre ID
        { this.setState({ id: this.props.navigation.getParam('ID', "") }) };
    }

    onRefresh() {
        this.setState({ refreshing: true });
        setTimeout(() => {
            this._getMovieInfo();
            this.setState({ refreshing: false });
        }, 1000)
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>
                    Movie Details
                </Text>
                <ScrollView
                    style={styles.scroll}
                    contentContainerStyle={styles.scrollContent}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={() => this.onRefresh()}
                        />
                    }
                >
                    {this._renderMovieDetail()}
                </ScrollView>
            </View>
        );
    }

    _renderMovieDetail() {
        return (
            <FlatList
                data={this.state.movie}
                keyExtractor={(item, index) => item.title}
                renderItem={this._renderMovieItem}
                ListEmptyComponent={this._renderEmptyList}
            />
        );
    }

    _renderMovieItem = ({ item }) => {
        return (
            <View>
                <Text>
                    Title: {item.getTitle()}
                </Text>
            </View>
        );
    }

    _getMovieInfo() {
        movieService.getMovieInfo(this.state.id)
            .then(results => {
                console.log('results:', results)
                this.setState({ movie: results });
            })
            .catch(error => {
                console.log('Something went wrong in genre!');
            })
    }

    componentDidMount() {
        console.log('home: componentDidMount');
        // this._getMovieInfo(); need to get info for loop does not work
    }

    /*** UPDATING ***/
    componentWillReceiveProps(nextProps) {
        console.log('home: componentWillReceiveProps (nextProp.custom: ' + nextProps.navigation.getParam('custom') + ')');
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log('home: shouldComponentUpdate');
        return true;
    }

    componentWillUpdate(nextProps, nextState) {
        console.log('home: componentWillUpdate');
    }

    //render() is the next step, but is defined already

    componentDidUpdate(prevProps, prevState) {
        console.log('home: componentDidUpdate');
    }

    /*** UNMOUNTING ***/
    componentWillUnmount() {
        console.log('home: componentWillUnmount');
    }
}
