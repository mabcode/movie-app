/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Text,
    TouchableOpacity,
    View,
    FlatList,
    RefreshControl,
    ScrollView,
    AlertIOS,
} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import movieService from '../services/movie.service';

import styles from '../styles/easyStyle';

export default class BrowsePage extends Component {
    static navigationOptions = {
        headerTitle: 'Browse',
        title: 'Browse'
    }

    constructor(props) {
        super(props);

        this.state = {
            refreshing: false,
            value: 1000,
            genres: [],
        }
    }

    /*** Mounting ***/
    componentWillMount() {
        console.log('Browse: componentWillMount');
    }

    onRefresh() {
        this.setState({ refreshing: true });
        setTimeout(() => {
           // this._getMovies();
            this._getGenres();
            this.setState({ refreshing: false });
        }, 1000)
    }

    render() {
        return (
            <View style={styles.container}>
                <NavigationEvents
                    onWillBlur={payload => console.log('Browse: willBlur', payload)}
                    onDidBlur={payload => console.log('Browse: didBlur', payload)}
                    onWillFocus={payload => console.log('Browse: willFocus', payload)}
                    onDidFocus={payload => console.log('Browse: didFocus', payload)}
                />
                <Text style={styles.welcome}>
                    Movie Genres
                </Text>

                <ScrollView
                    style={styles.scroll}
                    contentContainerStyle={styles.scrollContent}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={() => this.onRefresh()}
                        />
                    }
                >
                    {this._renderGenres()}
                </ScrollView>
            </View>
        );
    }

    _getGenres() {
        movieService.getGenres()
            .then(results => {
                console.log('results:', results)
                this.setState({ genres: results });
            })
            .catch(error => {
                console.log('Something went wrong in genre!');
            })
    }

    _renderGenres() {
        return (
            <FlatList
                data={this.state.genres}
                keyExtractor={(item, index) => item.title}
                renderItem={this._renderGenresItem}
                ListEmptyComponent={this._renderEmptyList}
            />
        );
    }

    _renderGenresItem = ({ item }) => {
        return (
            <View>
                <TouchableOpacity
                    onPress={() => { 
                        this.props.navigation.navigate('MovieSummary',{ 
                            genreID: item.getID(),
                            title: "Movie List Per Genre:"
                        }) 
                    }}
                    style={styles.touchableSmallButton}
                >
                    <Text
                        style={styles.item}
                    >
                        {item.getTitle()}
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }

    _renderEmptyList = () => {
        return (
            <Text>...Just a few more seconds</Text>
        );
    }

    componentDidMount() {
        console.log('Browse: componentDidMount');
        this._getGenres();
    }

    /*** UPDATING ***/
    componentWillReceiveProps(nextProps) {
        console.log('Browse: componentWillReceiveProps');
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log('Browse: shouldComponentUpdate');
        return true;
    }

    componentWillUpdate(nextProps, nextState) {
        console.log('Browse: componentWillUpdate');
    }

    //render() is the next step, but is defined already

    componentDidUpdate(prevProps, prevState) {
        console.log('Browse: componentDidUpdate');
    }

    /*** UNMOUNTING ***/
    componentWillUnmount() {
        console.log('Browse: componentWillUnmount');
    }
}
