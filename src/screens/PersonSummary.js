
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';

import styles from '../styles/easyStyle';

export default class PersonSummary extends Component {
    static navigationOptions = {
        title: 'Person Summary'
    }

    constructor(props) {
        super(props);
    }

    /*** Mounting ***/
    componentWillMount() {
        console.log('Home: componentWillMount');
    }

    render() {
        return (
            <View style={styles.container}>
               
                    <Text>
                        you here
                    </Text>
              
            </View>
        );
    }

    componentDidMount() {
        console.log('home: componentDidMount');
    }

    /*** UPDATING ***/
    componentWillReceiveProps(nextProps) {
        console.log('home: componentWillReceiveProps (nextProp.custom: ' + nextProps.navigation.getParam('custom') + ')');
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log('home: shouldComponentUpdate');
        return true;
    }

    componentWillUpdate(nextProps, nextState) {
        console.log('home: componentWillUpdate');
    }

    //render() is the next step, but is defined already

    componentDidUpdate(prevProps, prevState) {
        console.log('home: componentDidUpdate');
    }

    /*** UNMOUNTING ***/
    componentWillUnmount() {
        console.log('home: componentWillUnmount');
    }
}
