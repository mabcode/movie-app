import React, { Component } from 'react';
import {
    ScrollView,
    Image,
    RefreshControl,
    FlatList,
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import movieService from '../services/movie.service';

import styles from '../styles/easyStyle';


export default class MovieSummary extends Component {

    constructor(props) {
        super(props);

        this.state = {
            genre: 0,
            movies: [],
        }
    }

    static navigationOptions = {
        title: 'Movie Summary',
    }

    /*** Mounting ***/
    componentWillMount() {
        console.log('Two: componentWillMount');
        //set Genre ID
        { this.setState({ genre: this.props.navigation.getParam('genreID', "") }) };
    }

    onRefresh() {
        this.setState({ refreshing: true });
        setTimeout(() => {
            this._getMoviesGenres();
            this.setState({ refreshing: false });
        }, 1000)
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>
                    {this.props.navigation.getParam('title', "")}
                </Text>
                <ScrollView
                    style={styles.scroll}
                    contentContainerStyle={styles.scrollContent}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={() => this.onRefresh()}
                        />
                    }
                >
                    {this._renderMovieGenres()}
                </ScrollView>
            </View>
        );
    }

    _getMoviesGenres() {
        movieService.getMoviesPerGenre(this.state.genre)
            .then(results => {
                console.log('results:', results)
                this.setState({ movies: results });
            })
            .catch(error => {
                console.log('Something went wrong in genre!');
            })
    }


    _renderMovieGenres() {
        return (
            <FlatList
                data={this.state.movies}
                keyExtractor={(item, index) => item.title}
                renderItem={this._renderGenresItem}
                ListEmptyComponent={this._renderEmptyList}
            />
        );
    }

    _renderGenresItem = ({ item }) => {
        return (
            <View >
                <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.navigate('MovieDetail', {
                            _movie: item,
                        })
                    }}
                    style={styles.touchableSmallButton}
                >

                    <Text >
                        {this._renderImage(item.getImage())}
                    </Text>

                    <View>
                        <Text>
                            Title: {item.getTitle()}
                        </Text>
                        <View>
                            <Text>
                                Popularity: {item.getPopularity()}
                            </Text>
                        </View>
                        <View>
                            <Text>
                                Release Date: {item.getReleaseDate()}
                            </Text>
                        </View>
                        <View>
                            <Text style={{ padding: 8 }}>

                                Overview:
                        </Text>
                            <View>
                                <Text >

                                    {item.getOverview()}
                                </Text>
                            </View>
                        </View>


                    </View>
                </TouchableOpacity>
            </View>
        );
    }

    _renderImage(image) {
        return (
            <Image
                style={{ width: 70, height: 70, padding: 15 }}
                source={{ uri: 'https://image.tmdb.org/t/p/w154/' + image }}
            />
        )
    }



    componentDidMount() {
        console.log('Two: componentDidMount');
        console.log(this.state.genre)
        this._getMoviesGenres();

        this.navigationListeners = [
            this.props.navigation.addListener(
                'willBlur',
                data => {
                    console.log('Two: willBlur');
                }
            ),
            this.props.navigation.addListener(
                'didBlur',
                data => {
                    console.log('Two: didBlur');
                }
            ),
            this.props.navigation.addListener(
                'willFocus',
                data => {
                    console.log('Two: willFocus');
                }
            ),
            this.props.navigation.addListener(
                'didFocus',
                data => {
                    console.log('Two: didFocus');
                }
            )
        ]
    }

    /*** UPDATING ***/
    componentWillReceiveProps(nextProps) {
        console.log('Two: componentWillReceiveProps');
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log('Two: shouldComponentUpdate');
        return true;
    }

    componentWillUpdate(nextProps, nextState) {
        console.log('Two: componentWillUpdate');
    }

    //render() is the next step, but is defined already

    componentDidUpdate(prevProps, prevState) {
        console.log('Two: componentDidUpdate');
    }

    /*** UNMOUNTING ***/
    componentWillUnmount() {
        console.log('Two: componentWillUnmount');

        this.navigationListeners.forEach(item => {
            item.remove()
        });
    }
}