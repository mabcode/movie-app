/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';

import styles from '../styles/easyStyle';

export default class HomeScreen extends Component {
    static navigationOptions = {
        title: 'Home'
    }

    constructor(props) {
        super(props);
    }

    /*** Mounting ***/
    componentWillMount() {
        console.log('Home: componentWillMount');
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>
                    Moo-Vee-May-Knee-Uh
                </Text>
                <TouchableOpacity
                    onPress={() => { this.props.navigation.navigate('Search') }}
                    style={styles.touchableButton}
                >
                    <Text
                        style={styles.touchableButtonText}
                    >
                        Go to Search
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => { this.props.navigation.navigate('Browse') }}
                    style={styles.touchableButton}
                >
                    <Text
                        style={styles.touchableButtonText}
                    >
                        Go to Browse
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }

    componentDidMount() {
        console.log('home: componentDidMount');
    }

    /*** UPDATING ***/
    componentWillReceiveProps(nextProps) {
        console.log('home: componentWillReceiveProps (nextProp.custom: ' + nextProps.navigation.getParam('custom') + ')');
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log('home: shouldComponentUpdate');
        return true;
    }

    componentWillUpdate(nextProps, nextState) {
        console.log('home: componentWillUpdate');
    }

    //render() is the next step, but is defined already

    componentDidUpdate(prevProps, prevState) {
        console.log('home: componentDidUpdate');
    }

    /*** UNMOUNTING ***/
    componentWillUnmount() {
        console.log('home: componentWillUnmount');
    }
}
