
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';

import styles from '../styles/easyStyle';

export default class PersonDetail extends Component {
    static navigationOptions = {
        title: 'Person Detail'
    }

    constructor(props) {
        super(props);
    }

    /*** Mounting ***/
    componentWillMount() {
        console.log('Home: componentWillMount');
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>
                    Person Details
                </Text>
            </View>
        );
    }

    componentDidMount() {
        console.log('home: componentDidMount');
    }

    /*** UPDATING ***/
    componentWillReceiveProps(nextProps) {
        console.log('home: componentWillReceiveProps (nextProp.custom: ' + nextProps.navigation.getParam('custom') + ')');
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log('home: shouldComponentUpdate');
        return true;
    }

    componentWillUpdate(nextProps, nextState) {
        console.log('home: componentWillUpdate');
    }

    //render() is the next step, but is defined already

    componentDidUpdate(prevProps, prevState) {
        console.log('home: componentDidUpdate');
    }

    /*** UNMOUNTING ***/
    componentWillUnmount() {
        console.log('home: componentWillUnmount');
    }
}
