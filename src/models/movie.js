


export class Movie {
    constructor(title, id, image, popularity, releaseDate, overview) {
        this.title = title;
        this.id = id;
        this.popularity = popularity;
        this.releaseDate = releaseDate;
        this.overview = overview;
        this.image = image;
    }

    getImage() {
        return this.image;
    }

    getTitle() {
        return this.title;
    }

    getPopularity() {
        return this.popularity;
    }

    getReleaseDate() {
        return this.releaseDate;
    }

    getOverview() {
        return this.overview;
    }

    getID() {
        return this.id;
    }
}
