

export class Person {
    constructor(name,popularity,image) {
        this.name = name;
        this.popularity = popularity;
        this.image = image;
    }

    getName() {
        return this.name;
    }

    getPopularity() {
        return this.popularity;
    }

    getImage() {
        return this.image;
    }


}