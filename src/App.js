/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { createAppContainer, createBottomTabNavigator, createStackNavigator } from 'react-navigation'

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import Home from './screens/home';
import MovieSummary from './screens/MovieSummary';
import MovieDetail from './screens/MovieDetail';
import PersonSummary from './screens/PersonSummary';
import PersonDetail from './screens/PersonDetail';
import SearchPage from './screens/searchPage';
import BrowsePage from './screens/browsePage';

export default class App extends Component {

  render() {
    return (
      <AppContainer />
    );
  }
}

const searchStack = createStackNavigator(
  {
    Search: SearchPage,
    MovieSummary: MovieSummary,
    MovieDetail: MovieDetail,
    PersonSummary: PersonSummary,
    PersonDetail: PersonDetail,

  }
)

const browseStack = createStackNavigator(
  {
    Browse: BrowsePage,
    MovieSummary: MovieSummary,
    MovieDetail: MovieDetail,
    
  }

)

const Root = createBottomTabNavigator(
  {
    Home: Home,
    Browse: browseStack,
    Search: searchStack,
  },

  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let IconComponent = MaterialIcons;
        let iconName;
        if (routeName === 'Search') {
          iconName = 'search';
        } else if (routeName === 'Browse') {
          iconName = 'menu';
        } else if (routeName === 'Home') {
          iconName = 'home';
        }
        // You can return any component that you like here!
        return <IconComponent name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'blue',
      inactiveTintColor: 'gray',
    },
  }
);

const AppContainer = createAppContainer(Root);
